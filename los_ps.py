#TODO:
#allow command line arguments or something
#fix filename stuff
#allow for the specification of an identifier for each sim - some prefix added
#to each file to allow for easy identification, plotting, whatever
#make the data that are plotted into a .npy or .dat or something to allow for 
#plots additional to what are made to be easily created after
#add an option to generate mollweide plots of each mock

'''=============================================================================
Code to determine the momentum power spectrum of an n-body simulation
Takes as input the sim in question, loaded by pynbody, and creates plots of the
PS of the full sim and mock surveys created using sim data.
On the off chance that someone actually uses this and finds issues, email me at
curtis.millen@uq.net.au

In addition to the power spectrum of the full sim, this code can also generate
mock surveys that resemble the TAIPAN survey and determine the power spectrum
of these. This is achieved using the "zfull.npy" file included with this code,
which is a 3 column file containing redshift, their corresponding distances and
the coefficient which is a redshift-dependent probability of a galaxy being
randomly chosen.

Because this script calls the Fortran code POWMES, it will need to be run on a
node with compatible architecture. On dogmatix this is achieved by adding the
line "#SBATCH --constraint=FC430" to the submission script.

This code is run with "python los_ps.py [--template] filename".
The --template optional command will generate a template config file with name
filename.py and a SLURM script with name filename.sh. The template will need to
be altered and the SLURM script should just work.
If --template is not used, the code will take the config file with
name filename and run the code with the parameters contained within.
Note that filename should not contain any extension, this will be sorted out
by the program.
============================================================================='''
from __future__ import division
import matplotlib
matplotlib.use("pdf")


import numpy as np
import pynbody as pnb
import matplotlib.pyplot as plt
from matplotlib import rc
import os
from los_ps_support import *
import argparse
import importlib

#make plots look nice with LaTeX documents/in general
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)
plt.rcParams['lines.linewidth']=0.8
plt.rcParams['errorbar.capsize']=1.5

#======read in arguments========================================================
parser = argparse.ArgumentParser()
parser.add_argument('cf',help='''The name of the config file to use, or if the
option --template is used, the name to give the template file.''')
parser.add_argument('--template',help='generate template file',
                    action='store_true')

args = parser.parse_args()
if args.template:
    gen_config(args.cf)
    gen_slurm(args.cf)
    exit(0)
else:
    mod = importlib.import_module(args.cf)
    globals().update(mod.__dict__)

if not os.path.exists(outdir):
    os.system("mkdir {0}".format(outdir)) #make the output directory if it
                                          #doesnt already exist
sim = pnb.load(sim) #load the sim

#======stuff for testing========================================================
'''
#set all to True for the full analysis
write_file = False
do_powmes = False
powmes_plot = False
generate_mock = False
mock_powmes = False
mock_powmes_plot = True
save_plot_data = True

#======stuff for user to change=================================================

#load in the sim
#will need to be able to specify which sim in the future
sim = pnb.load("/data/s4353278/Nbody/lambda/output_00021/")
#sim = pnb.load("/data/s4353278/Nbody/frc3/output_00023/")
#sim = pnb.load("/data/s4353278/Nbody/frc4/output_00023/")

outdir = ("/data/s4353278/outputs/lambda/")#directory to output everything to
#outdir = ("/data/s4353278/outputs/frc3/")
#outdir = ("/data/s4353278/outputs/frc4/")

powmesloc = "/home/s4353278/powmes/powmes" #location of POWMES to determine PS

powmes_gridpoints = np.float(256) #number of gridpoints to use in POWMES

identifier = "lambda"
#identifier = "frc3"
#identifier = "frc4"
#will be prepended to every output file for easy identification
'''
'''=============================================================================
Unless something is broken, you shouldn't have to touch anything below this
============================================================================='''

file_prefix = outdir+identifier+'_'

#======setting up sim for analysis==============================================

#convert velocities to km/s but keep distances in "boxlengths"
sim.physical_units(sim.infer_original_units("Mpc"), "km s**-1")
boxsize = sim.properties.get("boxsize").in_units("Mpc") #boxlength in Mpc
centre = np.array([0.5,0.5,0.5]) #centre of box in normalised units
num_particles = len(sim)


#===============================================================================

#Get positions and LOS velocities out of sim
if write_file:
    master = open(file_prefix + "master.csv", 'w+')
    master.write("{0}\n".format(num_particles)) #write npart to first line for POWMES
    for i in range(len(sim)):
        pos = np.array(sim['pos'][i]) #position vector
        vel = np.array(sim['vel'][i]) #velocity vector
        vlos = make_los(pos,vel,centre) #LOS velocity
        #write x,y,z,v_los to file
        master.write("{0},{1},{2},{3}\n".format(pos[0],pos[1],pos[2],vlos))
    master.close() #will open in read mode later to generate mocks
del sim #close the sim to free up RAM, everything can be obtained from file
            #and we need the file for POWMES anyway


#======run powmes on our shiny new file=========================================
#make a config file for powmes to run
if do_powmes:
    make_powmes_config(file_prefix + "powmes.config",file_prefix + "master.csv",
                       file_prefix + "powmes_out.dat")

    os.system(powmesloc + " " + file_prefix + "powmes.config") #run powmes

#======plot powmes output - David's? code=======================================
if powmes_plot:
    gridpoints = powmes_gridpoints
    ps_f = open(file_prefix + "powmes_out.dat")
    pk_read_in = powerspec('l')
    lines = ps_f.readlines()
    for line in lines:
        c = line.split()
        mode = np.float(c[0])
        N_modes = np.float(c[1])
        PP_rough = np.float(c[2])
        P_debiased = np.float(c[3])
        shot_noise = np.float(c[4])
        dp_rough = np.float(c[5])
        k_in = np.float(mode)*(2.0*np.pi/np.float(boxsize))
        P_debiased = P_debiased - shot_noise/num_particles
        Pk_in = P_debiased*np.float(gridpoints)/((2.0*np.pi/np.float(boxsize))**3)
    #    print k_in, Pk_in
        sig_in = dp_rough*PP_rough
        if mode != 0:
            pk_read_in.add_k_value(k_in,Pk_in,shot_noise,dp_rough)
    if save_plot_data:
        savefile = np.zeros((len(pk_read_in.k),2))
        savefile[:,0] = pk_read_in.k
        savefile[:,1] = pk_read_in.Pk
        np.save(file_prefix + "full_plot_data.npy",savefile)
        
    plt.figure()
    plt.loglog(pk_read_in.k,pk_read_in.Pk)
    plt.title('Full Sim PS')
    plt.xlabel(r'k $(h/{\rm Mpc})$')
    plt.ylabel(r'P(k) kms$^{-1}(h/{\rm Mpc})^3$')

    plt.savefig(file_prefix + 'full_sim_ps.pdf')
    ps_f.close()

#======generate mocks===========================================================
for i in range(Nmocks):
    if generate_mock:
        mf = open(file_prefix + "master.csv", 'r')
        mockf = open(file_prefix + "mock_{0}.csv".format(i),'w+')
        temp = np.load("zfull.npy")
        zs = temp[:,0]
        dists = temp[:,1]
        coeffs = temp[:,2]
        
        #we need to make it so that the number of particles is about 50k
        #regardless of input size. Note that we are sampling half of the sky
        coeffs *= (500/77)*100000/(sum(coeffs)*num_particles)*(boxsize**3/((4/3)*np.pi*dists[-1]**3))
        
        p = True
        lines = mf.readlines()
        outlist = [] #not ideal but probably faster than writing a new file
        for line in lines[1:]: #first line contains num_particles so exclude it
            c = line.split(',') #read a line of the csv file
            x = float(c[0]) 
            y = float(c[1])
            z = float(c[2])
            vlos = float(c[3])
            #change units to Mpc and shift so that the centre is the origin
            cx = (x-0.5)*boxsize
            cy = (y-0.5)*boxsize
            cz = (z-0.5)*boxsize

            r = np.sqrt(cx*cx+cy*cy+cz*cz)
            if p:
                print r
                p = False
            
            if r>float(dists[-1]): #then it is outisde of TAIPAN's range
                continue #move on to next galaxy
            for j in range(len(zs)):
                if r>float(dists[j]): #it is further away than this redshift
                    continue
                else:
                    zbin=j #it is in this bin, write to array
                    if np.random.random()<coeffs[j]:
                        outlist.append([x,y,z,vlos])
                        break
        mn_part = len(outlist)
        mockf.write("{0}\n".format(mn_part))
        for i in range(len(outlist)):
            x = outlist[i][0]
            y = outlist[i][1]
            z = outlist[i][2]
            vlos = outlist[i][3]
            mockf.write("{0},{1},{2},{3}\n".format(x,y,z,vlos))
        del outlist
        del lines
        mf.close()
        mockf.close()        

#======run POWMES on those======================================================
for i in range(Nmocks):
    if mock_powmes:
        make_powmes_config(file_prefix + "powmes_mock_{0}.config".format(i),
                           file_prefix + "mock_{0}.csv".format(i),
                           file_prefix + "powmes_mock_{0}_out.dat".format(i))
        os.system(powmesloc + " " + file_prefix + "powmes_mock_{0}.config".format(i)) #run powmes
        

#======plot their power spectra=================================================
for i in range(Nmocks):
    if not generate_mock: #need to get number of particles from mock.csv
        tf = open(file_prefix + "mock_{0}.csv".format(i))
        mn_part = int(tf.readline())

    if mock_powmes_plot:
        gridpoints = powmes_gridpoints
        ps_f_mock = open(file_prefix + "powmes_mock_{0}_out.dat".format(i))
        pk_read_in_mock = powerspec('l')
        lines = ps_f_mock.readlines()
        for line in lines:
            c = line.split()
            mode = np.float(c[0])
            N_modes = np.float(c[1])
            PP_rough = np.float(c[2])
            P_debiased = np.float(c[3])
            shot_noise = np.float(c[4])
            dp_rough = np.float(c[5])
            k_in = np.float(mode)*(2.0*np.pi/np.float(boxsize))
            P_debiased = P_debiased - shot_noise/mn_part
            Pk_in = P_debiased*np.float(gridpoints)/((2.0*np.pi/np.float(boxsize))**3)
        #    print k_in, Pk_in
            sig_in = dp_rough*PP_rough
            if mode != 0:
                pk_read_in_mock.add_k_value(k_in,Pk_in,shot_noise,dp_rough)
        if save_plot_data:
            savefile = np.zeros((len(pk_read_in_mock.k),2))
            savefile[:,0] = pk_read_in_mock.k
            savefile[:,1] = pk_read_in_mock.Pk
            np.save(file_prefix + "mock_plot_data_{0}.npy".format(i),savefile)

        plt.figure()
        plt.loglog(pk_read_in_mock.k,pk_read_in_mock.Pk)
        plt.title('Mock PS {0}'.format(i))
        plt.xlabel(r'k $(h/{\rm Mpc})$')
        plt.ylabel(r'P(k) kms$^{-1}(h/{\rm Mpc})^3$')

        plt.savefig(file_prefix+'mock_ps_{0}.pdf'.format(i))
        ps_f_mock.close()
