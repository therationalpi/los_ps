# README #
### What is this repository for? ###

Code to determine the line-of-sight momentum power spectrum of an N-body 
simulation. It will also generate mock surveys and determine power spectra of
those.

### How do I get set up? ###

Requires Python 2.7 and with numpy, matplotlib and pynbody.
Also requires the FORTRAN code POWMES to be compiled somewhere
(http://www.projet-horizon.fr/article345.html)
Oh, you'll also probably want an N-body simulation. This was run on simulations
in the RAMSES format and I think pynbody supports others but I make no
guarantees.

The program takes a filename (without an extension) as a parameter, and running
with the --template optional argument will generate a config file and SLURM
script that will run the program. Both of these will need to be modified for
your specific use case.

Any problems can be reported to me at curtis.millen@uq.net.au
