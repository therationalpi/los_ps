from __future__ import division
import numpy as np

#======classes==================================================================

class powerspec():

    def __init__(self,label):
        """Set up a data Structure
        """
        self.label=label
        self.k = []
        self.Pk =[]
        self.W=[]
        self.sig_Pk=[]
        

    def add_k_value(self,k_in,Pk_in,W_in,sig_in):
        """Add a k-value
        """
        self.k.append(k_in)
        self.Pk.append(Pk_in)
        self.W.append(W_in)
        self.sig_Pk.append(sig_in)

#======functions================================================================

def make_los(pos, vel, testpoint):
    '''
    make_los(np.array(3),np.array(3),np.array(3)) -> float
    determine the magnitude of the LOS velocity from the point testpoint
    '''
    vec = pos - testpoint
    unit = make_unit(vec)
    vlos = np.dot(unit, vel)
    return vlos

def make_unit(vec):
    '''
    make_unit(array) -> array
    converts vec into its corresponding unit vector
    '''
    unit = vec / np.dot(vec, vec)
    return unit

def make_powmes_config(fileconfig, filein, fileout):
    f = open(fileconfig,'w')
    f.write('''&input
 verbose=.true.
 megaverbose=.true.
 filein='{0}'
 nfile=3
 nmpi=96
 read_mass=.true.
 nfoldpow=-256
 ngrid=256
 norder=3
 shift=0.0 0.0 0.0
 filepower='{1}'
 filepower_fold='#powspec'
 filetaylor='#powspec.taylor'
/'''.format(filein,fileout))

def gen_config(fname):
    f = open(fname+'.py',"w+")
    f.write('''
write_file = True                   #write the .csv for the whole sim
do_powmes = True                    #generate the momentum ps for the whole sim
powmes_plot = True                  #plot the momentum ps for the whole sim
generate_mock = True                #generate a number of mock surveys specified by Nmocks
mock_powmes = True                  #generate the momentum ps of each mock
mock_powmes_plot = True             #plot them
save_plot_data = True               #save the plot data as .npy files for later use
Nmocks = 1                          #number of mocks to generate
sim = "path/to/sim"                 #where the simulation data it located
outdir = "path/to/output/directory" #where the results should be ouput to
powmesloc = "path/to/powmes/binary" #where the powmes binary is located
powmes_gridpoints = 256             #number of gridpoints used by powmes
identifier = "something"            #identifying string to prepend to each output file

''')

def gen_slurm(fname):
    f = open(fname+'.sh',"w+")
    f.write('''#!/bin/bash
#SBATCH -J full
#SBATCH --partition smp
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --mem=32G
#SBATCH --time=40:00:00
#SBATCH --constraint=FC430
#SBATCH -o full.out
#SBATCH -e full.err

cd ~/nbodyanalysis/fullanalysis/
module load rocks-openmpi
module load python/2.7.10

python los_ps.py {0}

echo "Program finished with exit code $? at: `date`"
'''.format(fname))

    




    
